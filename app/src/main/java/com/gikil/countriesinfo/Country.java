package com.gikil.countriesinfo;

import android.support.annotation.NonNull;

public class Country implements Comparable{

    private String countryId;   // BA
    private String countryName;  // Bosnia and Herzegovina
    private String capitalCityName; // Sarajevo
    private String telephoneCountryCode;  // 387
    private String countryWebInfo;  // web link
    private String imgFlagWebLink;

    public Country() {
    }

    public Country(String countryId, String countryName, String capitalCityName, String telephoneCountryCode, String countryWebInfo, String imgFlagWebLink) {
        this.countryId = countryId;
        this.countryName = countryName;
        this.capitalCityName = capitalCityName;
        this.telephoneCountryCode = telephoneCountryCode;
        this.countryWebInfo = countryWebInfo;
        this.imgFlagWebLink = imgFlagWebLink;
    }

    public String getCountryId() {
        return countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getCapitalCityName() {
        return capitalCityName;
    }

    public String getTelephoneCountryCode() {
        return telephoneCountryCode;
    }

    public String getCountryWebInfo() {
        return countryWebInfo;
    }

    public String getImgFlagWebLink() {
        return imgFlagWebLink;
    }

    @Override
    public String toString() {
        return "Country{" +
                "countryId='" + countryId + '\'' +
                ", countryName='" + countryName + '\'' +
                ", capitalCityName='" + capitalCityName + '\'' +
                ", telephoneCountryCode='" + telephoneCountryCode + '\'' +
                ", countryWebInfo='" + countryWebInfo + '\'' +
                ", imgFlagWebLink='" + imgFlagWebLink + '\'' +
                '}';
    }

    @Override
    public int compareTo(@NonNull Object o) {
        Country country = (Country) o;
        return getCountryName().compareTo(country.getCountryName());
    }
}
