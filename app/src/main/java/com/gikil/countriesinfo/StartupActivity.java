package com.gikil.countriesinfo;

import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class StartupActivity extends AppCompatActivity {

    private Button btnDownload;
    private Button btnExit;
    private ListView countryList;
    private TextView title;
    private TextView countryId;
    private TextView countryName;
    private TextView countryCapitalName;
    private TextView countryTelCode;
    private TextView countryWebInfo;
    private ImageView countryFlag;
    private RelativeLayout countryInfoLayout;
    private Button btnClose;
    private RelativeLayout backgroundLayout;

    private ArrayAdapter adapter;
    private ArrayList<String> data;
    private ArrayList<Country> countries = new ArrayList<>();

    private MediaPlayer mp;
    private DownloadClass task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);

        initialize();

        countryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mp.start();
                backgroundLayout.setVisibility(View.VISIBLE);
                countryList.setAlpha(0.5f);
                setCountryInfo(position);
                countryList.setClickable(false);
                countryList.setEnabled(false);
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.start();
                backgroundLayout.setVisibility(View.GONE);
                countryList.setClickable(true);
                countryList.setEnabled(true);
                countryList.setAlpha(1);
            }
        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.start();
                StartupActivity.this.finish();
                System.exit(0);
            }
        });

    }

    private void setCountryInfo(int pos) {

        countryId.setText("Country code: "+countries.get(pos).getCountryId());
        countryName.setText("Country name: "+countries.get(pos).getCountryName());
        countryCapitalName.setText("Capital city: "+countries.get(pos).getCapitalCityName());
        countryTelCode.setText("Country phone code: "+countries.get(pos).getTelephoneCountryCode());
        countryWebInfo.setText("Country www info: "+countries.get(pos).getCountryWebInfo());
        setCountryFlag(countries.get(pos).getImgFlagWebLink());
    }

    private void setCountryFlag(String url) {

        try {
            Picasso.with(StartupActivity.this)
                    .load(url)
                    .placeholder(R.drawable.placeholder_flag)
                    .error(R.drawable.img_error)
                    .into(countryFlag);
        }
        catch (Exception e) {
            Toast.makeText(StartupActivity.this,"Bad image link!",Toast.LENGTH_SHORT).show();
        }

    }

    private void initialize() {
        btnDownload = (Button) findViewById(R.id.btnDownload);
        btnExit = (Button) findViewById(R.id.btnExit);
        countryList = (ListView) findViewById(R.id.listOfCountries);
        title = (TextView) findViewById(R.id.txtTitle);
        backgroundLayout= (RelativeLayout) findViewById(R.id.backgroundLayout);

        countryId = (TextView) findViewById(R.id.txt_country_id);
        countryName = (TextView) findViewById(R.id.txt_country_name);
        countryCapitalName = (TextView) findViewById(R.id.txt_country_capital);
        countryTelCode = (TextView) findViewById(R.id.txt_country_tel_code);
        countryWebInfo = (TextView) findViewById(R.id.txt_country_web_info);
        countryInfoLayout = (RelativeLayout) findViewById(R.id.countryInfoLayout);
        btnClose = (Button) findViewById(R.id.btnClose);
        countryFlag = (ImageView) findViewById(R.id.img_flag);

        data = new ArrayList<>();
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, data);
        countryList.setAdapter(adapter);

        mp=MediaPlayer.create(this,R.raw.button_click);
        backgroundLayout.setVisibility(View.GONE);
        title.setVisibility(View.GONE);
    }

    public void downloadCountryNames(View view) {
        mp.start();
        title.setVisibility(View.VISIBLE);
        task = new DownloadClass();
        try {
            task.execute("http://www.geognos.com/api/en/countries/info/all.json").get();
        } catch (Exception e) {
            Toast.makeText(StartupActivity.this,"Wrong JSON link",Toast.LENGTH_SHORT).show();
        }
        btnDownload.setVisibility(View.GONE);
    }

    public class DownloadClass extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {

            // Making HTTP request
            BufferedReader reader;
            StringBuilder sb= new StringBuilder();
            String line = null;
            URL url;
            HttpURLConnection httpURLConnection = null;
            InputStream inputStream;

            try {
                url = new URL(params[0]);
                httpURLConnection = (HttpURLConnection) url.openConnection();
                inputStream = httpURLConnection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(inputStream));
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                inputStream.close();

            } catch (Exception e) {
                Toast.makeText(StartupActivity.this,"Buffer Error , Error converting result!",Toast.LENGTH_SHORT).show();
            }

            // return JSON String
            return sb.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            String capitalCityName="";
            String countryId="";

            JSONObject object = null;
            JSONObject results=null;
            JSONObject country=null;

            countries.clear();
            data.clear();

            try {
                object = new JSONObject(s);
                results=object.getJSONObject("Results");
                Iterator<String> keys = results.keys();

                while (keys.hasNext()) {
                    countryId= keys.next();
                    country=results.getJSONObject(countryId);
                    if(!country.isNull("Capital")) {
                        capitalCityName=country.getJSONObject("Capital").getString("Name");
                    }
                    else {
                        capitalCityName="";
                    }
                    countries.add(new Country(
                            countryId,
                            country.getString("Name"),
                            capitalCityName,
                            country.getString("TelPref"),
                            country.getString("CountryInfo"),
                            "http://www.geognos.com/api/en/countries/flag/"+countryId+".png")
                    );
                }
                Collections.sort(countries);
                for (Country c: countries) {
                    data.add(c.getCountryName());
                }
                adapter.notifyDataSetChanged();
                Toast.makeText(StartupActivity.this,"CountryListView contains : "+data.size()+" countries!",Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(StartupActivity.this, " Lista zemalja nije dostupna!", Toast.LENGTH_SHORT).show();
            }
        }
    }


}
